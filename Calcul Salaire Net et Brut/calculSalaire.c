#include <stdio.h>

int main() {
    float salaire_brut;
    float charges;
    float salaire_net;

    // Demandez à l'utilisateur de saisir son salaire brut mensuel
    printf("Veuillez entrer le montant de votre salaire brut mensuel : ");
    scanf("%f", &salaire_brut);

    // Calculez le montant des charges (25% du salaire brut)
    charges = salaire_brut * 0.25;

    // Calculez le salaire net en soustrayant les charges du salaire brut
    salaire_net = salaire_brut - charges;

    // Affichez le résultat
    printf("Salaire brut mensuel : %.2f €\n", salaire_brut);
    printf("Charges (25%%) : %.2f €\n", charges);
    printf("Salaire net mensuel : %.2f €\n", salaire_net);

    return 0;
}
