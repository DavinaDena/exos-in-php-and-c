<!DOCTYPE html>
<html>
<head>
    <title>Calculateur de Salaire Net/Brut</title>
</head>
<body>

<h1>Calculateur de Salaire Net/Brut</h1>

<form method="post" action="calculateur.php">
    Entrez votre salaire brut mensuel : <input type="text" name="salaire_brut">
    <input type="submit" value="Calculer">
</form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Récupérez le salaire brut depuis le formulaire
    $salaire_brut = floatval($_POST["salaire_brut"]);

    // Calculez le montant des charges (25% du salaire brut)
    $charges = $salaire_brut * 0.25;

    // Calculez le salaire net en soustrayant les charges du salaire brut
    $salaire_net = $salaire_brut - $charges;

    // Affichez le résultat
    echo "<h2>Résultats :</h2>";
    echo "Salaire brut mensuel : " . number_format($salaire_brut, 2) . " €<br>";
    echo "Charges (25%) : " . number_format($charges, 2) . " €<br>";
    echo "Salaire net mensuel : " . number_format($salaire_net, 2) . " €<br>";
}
?>

</body>
</html>
