<?php

// Fonction pour saisir les notes
function saisirNotes() {
    $notes = array();
    for ($i = 0; $i < 3; $i++) {
        $eleve = array();
        echo "Notes pour l'élève " . ($i + 1) . ":\n";
        for ($j = 0; $j < 4; $j++) {
            $note = floatval(readline("Note " . ($j + 1) . ": "));
            $eleve[] = $note;
        }
        $notes[] = $eleve;
    }
    return $notes;
}

// Fonction pour calculer la somme des notes
function sommeNotes($notes) {
    $sommes = array();
    foreach ($notes as $eleve) {
        $somme = array_sum($eleve);
        $sommes[] = $somme;
    }
    return $sommes;
}

// Fonction pour calculer la moyenne des notes d'un élève
function moyenneEleve($notes, $eleve) {
    $somme = array_sum($notes[$eleve]);
    return $somme / 4;
}

// Fonction pour calculer la moyenne de la classe
function moyenneClasse($notes) {
    $flattenNotes = array_merge(...$notes);
    $somme = array_sum($flattenNotes);
    return $somme / count($flattenNotes);
}

// Fonction pour calculer la note la plus basse et la note la plus haute
function noteMinMax($notes) {
    $flattenNotes = array_merge(...$notes);
    $minNote = min($flattenNotes);
    $maxNote = max($flattenNotes);
    return array($minNote, $maxNote);
}

// Fonction pour trier les notes de la plus petite à la plus grande
function trierNotes($notes) {
    foreach ($notes as &$eleve) {
        sort($eleve);
    }
}

// Saisir les notes
$notes = saisirNotes();

// Calculer la somme des notes
$sommes = sommeNotes($notes);

// Calculer la moyenne de la classe
$moyenneClasse = moyenneClasse($notes);

// Calculer la moyenne d'un élève (par exemple, élève 1)
$moyenneEleve1 = moyenneEleve($notes, 0);

// Trouver la note minimale et maximale
list($minNote, $maxNote) = noteMinMax($notes);

// Trier les notes
trierNotes($notes);

// Afficher les résultats
echo "\nSomme des notes par élève:\n";
for ($i = 0; $i < 3; $i++) {
    echo "Élève " . ($i + 1) . ": " . $sommes[$i] . "\n";
}

echo "\nMoyenne de la classe: " . $moyenneClasse . "\n";
echo "\nMoyenne des notes d'un élève (par exemple, élève 1): " . $moyenneEleve1 . "\n";
echo "\nNote la plus basse: " . $minNote . "\n";
echo "Note la plus haute: " . $maxNote . "\n";

echo "\nNotes triées de la plus petite à la plus grande:\n";
foreach ($notes as $key => $eleve) {
    echo "Élève " . ($key + 1) . ": " . implode(" ", $eleve) . "\n";
}

?>
