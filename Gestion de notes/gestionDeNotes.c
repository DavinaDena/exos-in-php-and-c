#include <stdio.h>

// Fonction pour saisir les notes
void saisirNotes(float notes[3][4]) {
    for (int i = 0; i < 3; i++) {
        printf("Notes pour l'élève %d:\n", i + 1);
        for (int j = 0; j < 4; j++) {
            printf("Note %d: ", j + 1);
            scanf("%f", &notes[i][j]);
        }
    }
}

// Fonction pour calculer la somme des notes pour chaque élève
void sommeNotes(float notes[3][4], float somme[3]) {
    for (int i = 0; i < 3; i++) {
        somme[i] = 0; // Initialisation de la somme à zéro
        for (int j = 0; j < 4; j++) {
            somme[i] += notes[i][j]; // Ajout de chaque note à la somme
        }
    }
}

// Fonction pour calculer la moyenne des notes d'un élève
float moyenneEleve(float notes[3][4], int eleve) {
    float somme = 0; // Initialisation de la somme à zéro
    for (int j = 0; j < 4; j++) {
        somme += notes[eleve][j]; // Ajout de chaque note à la somme
    }
    return somme / 4; // Calcul de la moyenne
}

// Fonction pour calculer la moyenne de la classe
float moyenneClasse(float notes[3][4]) {
    float somme = 0; // Initialisation de la somme à zéro
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            somme += notes[i][j]; // Ajout de chaque note à la somme
        }
    }
    return somme / (3 * 4); // Calcul de la moyenne
}

// Fonction pour trouver la note la plus basse et la note la plus haute
void minMaxNotes(float notes[3][4], float *minNote, float *maxNote) {
    *minNote = notes[0][0]; // Initialisation de la note minimale à la première note
    *maxNote = notes[0][0]; // Initialisation de la note maximale à la première note

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            if (notes[i][j] < *minNote) {
                *minNote = notes[i][j]; // Mise à jour de la note minimale si nécessaire
            }
            if (notes[i][j] > *maxNote) {
                *maxNote = notes[i][j]; // Mise à jour de la note maximale si nécessaire
            }
        }
    }
}

// Fonction pour trier les notes d'un élève en ordre croissant
void trierNotes(float notes[3][4]) {
    float temp;
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            for (int k = j + 1; k < 4; k++) {
                if (notes[i][j] > notes[i][k]) {
                    temp = notes[i][j];
                    notes[i][j] = notes[i][k];
                    notes[i][k] = temp;
                }
            }
        }
    }
}

int main() {
    float notes[3][4];
    float somme[3];
    float minNote, maxNote;

    saisirNotes(notes); // Saisir les notes
    sommeNotes(notes, somme); // Calculer la somme des notes
    minMaxNotes(notes, &minNote, &maxNote); // Trouver la note minimale et maximale

    printf("\nSomme des notes par élève:\n");
    for (int i = 0; i < 3; i++) {
        printf("Élève %d: %.2f\n", i + 1, somme[i]); // Afficher la somme des notes par élève
    }

    printf("\nMoyenne de la classe: %.2f\n", moyenneClasse(notes)); // Afficher la moyenne de la classe

    printf("\nMoyenne des notes d'un élève (par exemple, élève 1): %.2f\n", moyenneEleve(notes, 0)); // Afficher la moyenne d'un élève

    printf("\nNote la plus basse: %.2f\n", minNote); // Afficher la note minimale
    printf("Note la plus haute: %.2f\n", maxNote); // Afficher la note maximale

    trierNotes(notes); // Trier les notes d'un élève en ordre croissant
    printf("\nNotes triées de la plus petite à la plus grande:\n");
    for (int i = 0; i < 3; i++) {
        printf("Élève %d: ", i + 1);
        for (int j = 0; j < 4; j++) {
            printf("%.2f ", notes[i][j]); // Afficher les notes triées
        }
        printf("\n");
    }

    return 0;
}
