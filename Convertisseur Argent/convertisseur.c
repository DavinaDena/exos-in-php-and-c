#include <stdio.h>

float usd_to_eur(float usd_amount) {
    float conversion_rate = 0.85; // Taux de conversion USD à Euros
    return usd_amount * conversion_rate;
}

float eur_to_usd(float eur_amount) {
    float conversion_rate = 1.18; // Taux de conversion Euros à USD
    return eur_amount * conversion_rate;
}

int main() {
    int choice;
    float amount;

    printf("Convertisseur USD ↔ Euros\n");
    printf("1. USD à Euros (USD → €)\n");
    printf("2. Euros à USD (€ → USD)\n");

    printf("Choisissez l'option (1 ou 2) : ");
    scanf("%d", &choice);

    if (choice == 1) {
        printf("Entrez le montant en USD : $");
        scanf("%f", &amount);
        float eur_amount = usd_to_eur(amount);
        printf("$%.2f équivaut à €%.2f\n", amount, eur_amount);
    } else if (choice == 2) {
        printf("Entrez le montant en Euros : €");
        scanf("%f", &amount);
        float usd_amount = eur_to_usd(amount);
        printf("€%.2f équivaut à $%.2f\n", amount, usd_amount);
    } else {
        printf("Option non valide. Choisissez 1 pour USD → € ou 2 pour € → USD.\n");
    }

    return 0;
}
