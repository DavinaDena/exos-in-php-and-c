<?php

function usd_to_eur($usd_amount) {
    $conversion_rate = 0.85; // Taux de conversion USD à Euros
    $eur_amount = $usd_amount * $conversion_rate;
    return $eur_amount;
}

function eur_to_usd($eur_amount) {
    $conversion_rate = 1.18; // Taux de conversion Euros à USD
    $usd_amount = $eur_amount * $conversion_rate;
    return $usd_amount;
}

echo "Convertisseur USD ↔ Euros\n";
echo "1. USD à Euros (USD → €)\n";
echo "2. Euros à USD (€ → USD)\n";

$choice = readline("Choisissez l'option (1 ou 2) : ");

if ($choice == 1) {
    $usd_amount = readline("Entrez le montant en USD : $");
    $eur_amount = usd_to_eur($usd_amount);
    echo "\$$usd_amount équivaut à €$eur_amount\n";
} elseif ($choice == 2) {
    $eur_amount = readline("Entrez le montant en Euros : €");
    $usd_amount = eur_to_usd($eur_amount);
    echo "€$eur_amount équivaut à \$$usd_amount\n";
} else {
    echo "Option non valide. Choisissez 1 pour USD → € ou 2 pour € → USD.\n";
}

?>
